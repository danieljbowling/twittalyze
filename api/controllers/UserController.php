<?php

	namespace Twittalyze\controllers;
	
	use Twittalyze\models\User;
	require "models/User.php";

	class UserController{
		function processRoutes($_routes,$_callback){
			if( isset($_routes[0]) && $_routes[0] == "user" && isset($_routes[1] ) ){
				$oUser = new User($_routes[1]);
				if( isset($_routes[2]) && $_routes[2] == "timeline" ){
					$_callback(array("error"=>false,"timeline"=>$oUser->timeline()));
				}else if( isset($_routes[2]) && $_routes[2] == "test" ){
					$_callback(array("error"=>false,"test"=>$oUser->test()));
				}else{
					$_callback(array("error"=>false,"projects"=>$oUser->timeline()));
				}
			}
		}
	}

?>