<?php

	namespace Twittalyze\controllers;
	
	use Twittalyze\models\Place;
	require "models/Place.php";

	class PlaceController{
		function processRoutes($_routes,$_callback){
			if( isset($_routes[0]) && $_routes[0] == "place" && isset($_routes[1] ) ){
				
				if( isset($_routes[1]) && $_routes[1] == "trends" && isset($_routes[2]) && !isset($_routes[3]) ){
					$oPlace = new Place();
					$_callback( 
						array(
								"error"=>false
								,"trends"=>$oPlace->trends( $_routes[2] ) 
							)
					);
				}else if( isset($_routes[1]) && $_routes[1] == "trends" && isset($_routes[2]) && isset($_routes[3]) ){
					$oPlace = new Place();
					$_callback( 
						array(
								"error"=>false
								,"trends"=>$oPlace->locTrends( $_routes[2], $_routes[3] ) 
							)
					);

				}else if( isset($_routes[1]) && $_routes[1] == "geo" && isset($_routes[2]) && isset($_routes[3]) ){
					
					$oPlace = new Place();
					$_callback( 
						array(
								"error"=>false
								,"places"=>$oPlace->geo( $_routes[2], $_routes[3] ) 
							)
					);

				}else if( isset($_routes[1]) && $_routes[1] == "info" && isset($_routes[2]) ){

					$oPlace = new Place();
					$_callback( 
						array(
								"error"=>false
								,"info"=>$oPlace->info( $_routes[2] ) 
							)
					);

				}else if( isset($_routes[1]) ){
					$oPlace = new Place();
					$_callback(array("error"=>false,"data"=>$oPlace->info($_routes[1])));
				}else{
					$_callback(array("error"=>true));
				}
			}
		}
	}

?>