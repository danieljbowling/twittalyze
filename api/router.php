<?php

	namespace Twittalyze;

	class Router{

		function __construct(){
			// $this->uri = $this->getCurrentUri();
			$this->routes = $this->getRoutes();
		}

		function getCurrentUri(){
			$basepath = implode('/', array_slice(explode('/', $_SERVER['SCRIPT_NAME']), 0, -1)) . '/';
			$uri = substr($_SERVER['REQUEST_URI'], strlen($basepath));
			if (strstr($uri, '?')) $uri = substr($uri, 0, strpos($uri, '?'));
			$uri = '/' . trim($uri, '/');
			return $uri;
		}

		function getRoutes(){
			$base_url = $this->getCurrentUri();
			$routes = array();
			$routes = explode('/', $base_url);

			foreach($routes as $route){
				if(trim($route) != '')
					array_push($routes, $route);
			}

			return $routes;
		}

		function get($_params,$_func){
			$parts = explode("/", $_params);
			$routes = $this->getRoutes();
			array_shift( $routes );								// Drop the first guy
			$routes = array_slice($routes, count($routes)/2);	// Slice it in half. Seems to be duplicates in second half
			if( $routes[0] == $parts[0] ){
				$_func($routes);
			}
		}

	}
?>