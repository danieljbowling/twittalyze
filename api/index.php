<?php

	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);

	require_once 'router.php';
	require_once 'controllers/UserController.php';
	require_once 'controllers/PlaceController.php';

	use Twittalyze\Router;

	use Twittalyze\controllers\UserController;
	use Twittalyze\controllers\PlaceController;

	$router = new Router();
	header('Access-Control-Allow-Origin: *');

	$router->get("user",function($routes){		// They want one person
		$uc = new UserController();
		$uc->processRoutes($routes,function($response){
			echo json_encode($response);
		});
	});

	$router->get("place",function($routes){		// They want one person
		$pc = new PlaceController();
		$pc->processRoutes($routes,function($response){
			echo json_encode($response);
		});
	});

	$router->get("",function($routes){
?>
	
	<html>
		<body>

			<div>
				<h2>User Methods</h2>
				/api/user/[username]/timeline			- time line of last 50 tweets	<br/> 
			</div>
			<div>
				<h2>Place Methods</h2>
				/api/place/trends/[lat]/[lng]			- trends nearest that lat lng	<br/>
				/api/place/trends/[woeid]				- trends for that place 		<br/>
				/api/place/geo/[lat]/[lng]				- search places near that lat lng 	<br/>
			</div>
		</body>
	</html>

<?php
	});

?>