<?php

	namespace Twittalyze\models;

	use Twittalyze\call\TwitterBase;
	require_once "call/TwitterBase.php";

	class User extends TwitterBase{

		function __construct($_user){
			parent::__construct();
			$this->user = $_user;
		}

		function place(){
			$url = "https://api.twitter.com/1.1/geo/reverse_geocode.json";
			$request = array(
					'lat' => '-33.88638',
					'long' => '151.21108'
				);
		}

		function test(){
			$request = array(
				'id'   => 1
			);
			$url = "https://api.twitter.com/1.1/trends/place.json";

			$data = $this->call(array("url"=>$url,"request"=>$request));

			return array("data"=>$data);
		}

		function timeline(){

			$tweet_count = 50;
			$request = array(
				'screen_name'   => $this->user,
				'count'         => $tweet_count
			);
			$url = "https://api.twitter.com/1.1/statuses/user_timeline.json";

			$data = $this->call(array("url"=>$url,"request"=>$request));

			// $data = $this->timelineCall(array("user"=>$this->user));

			$tweets_hrs = array();
			for($i=0;$i!=count($data);++$i){
				$created_at = $data[$i]['created_at'];
				$dateObject = \DateTime::createFromFormat("D M d H:i:s O Y",$created_at);
				$hour = $dateObject->format('H');
				$tweets_hrs[$hour] = isset($tweets_hrs[$hour]) ? $tweets_hrs[$hour] + 1 : 1;
			}
			if( count($tweets_hrs) == 0 ){		// None.. empty... nothing to report sorry.
				return array();
			}

			$cron_data = array();		// All hours of the day. Good for a graph
			for($h=0;$h!=24;++$h){
				$hour = str_pad($h, 2, "0", STR_PAD_LEFT);
				if( isset($tweets_hrs[$hour]) ){
					$cron_data[$h] = $tweets_hrs[$hour];
				}else{
					$cron_data[$h] = 0;
				}
			}
			arsort($tweets_hrs);
			$rows = array();
			foreach($tweets_hrs as $hour=>$count){
				$rows[] = array("hour"=>$hour,"count"=>$count);
			}

			return array("user"=>$this->user,"tweet_count"=>$tweet_count,"ordered"=>$rows,"cron_data"=>$cron_data);
		}

	}

?>