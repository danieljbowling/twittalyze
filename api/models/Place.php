<?php

	namespace Twittalyze\models;

	use Twittalyze\call\TwitterBase;
	require_once "call/TwitterBase.php";

	class Place extends TwitterBase{

		function __construct(){
			parent::__construct();
		}

		function geo($_lat, $_lng){
			$url = "https://api.twitter.com/1.1/geo/reverse_geocode.json";
			$request = array(
					'lat' => $_lat,
					'long' => $_lng
				);
			$data = $this->call(array("url"=>$url,"request"=>$request));

			return array("data"=>$data);
		}

		function placeSearch($_lat, $_lng){
			$url = "https://api.twitter.com/1.1/geo/search.json";
			$request = array(
					'lat' => $_lat,
					'long' => $_lng
				);
			$data = $this->call(array("url"=>$url,"request"=>$request));

			return array("data"=>$data);
		}

		function locTrends($_lat, $_lng){
			$url = "https://api.twitter.com/1.1/trends/closest.json";
			$request = array(
					'lat' => $_lat,
					'long' => $_lng
				);
			$data = $this->call(array("url"=>$url,"request"=>$request));

			return array("data"=>$data);
		}

		function trends($_id){		//1105779
			$url = "https://api.twitter.com/1.1/trends/place.json";
			$request = array(
					'id' => $_id
				);
			$data = $this->call(array("url"=>$url,"request"=>$request));

			return array("data"=>$data);
		}

		function info($_place_id){	//43a8d924c3c6fb27

			$url = "https://api.twitter.com/1.1/geo/id/$_place_id.json";
			$request = array(
					'place_id' => $_place_id
				);
			$data = $this->call(array("url"=>$url,"request"=>$request));

			return array("data"=>$data);
		}

	}

?>