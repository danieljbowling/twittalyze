<?php

	namespace Twittalyze\call;

	class TwitterBase{

		function __construct(){
			$this->variables();
		}
		function variables(){
			$this->oauth_access_token = "393597256-Xl8SUvPLbv0NFcVnYokd1EUnrih8i4zl3KPjYcb3";
			$this->oauth_access_token_secret = "lRKQ3OXIESoxNHR9wjbEhEToNXnrbzp70cop89pozfCOs";
			$this->consumer_key = "OHo6zewLwHY9y2va4ROfmrxV1";
			$this->consumer_secret = "jmX7pnJ5t7aHuJpHObd7ws4w3ITnvxQz2DwiICT7ZV0VoBohii";   
		}

		function buildBaseString($baseURI, $method, $params) {
			$r = array();
			ksort($params);
			foreach($params as $key=>$value){
				$r[] = "$key=" . rawurlencode($value);
			}
			return $method."&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $r));
		}

		function buildAuthorizationHeader($oauth) {
			$r = 'Authorization: OAuth ';
			$values = array();
			foreach($oauth as $key=>$value)
				$values[] = "$key=\"" . rawurlencode($value) . "\"";
			$r .= implode(', ', $values);
			return $r;
		}

		function call($_params){
			$url = isset($_params['url']) ? $_params['url'] : "";
			$request = isset($_params['request']) ? $_params['request'] : [];

			$oauth = array(
				'oauth_consumer_key'        => $this->consumer_key,
				'oauth_nonce'               => time(),
				'oauth_signature_method'    => 'HMAC-SHA1',
				'oauth_token'               => $this->oauth_access_token,
				'oauth_timestamp'           => time(),
				'oauth_version'             => '1.0'
			);

			$oauth = array_merge($oauth, $request);	//  merge request and oauth to one array

			$base_info              = $this->buildBaseString($url, 'GET', $oauth);
			$composite_key          = rawurlencode($this->consumer_secret) . '&' . rawurlencode($this->oauth_access_token_secret);
			$oauth_signature            = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
			$oauth['oauth_signature']   = $oauth_signature;

			$header = array($this->buildAuthorizationHeader($oauth), 'Expect:');
			$options = array( CURLOPT_HTTPHEADER => $header,
							  CURLOPT_HEADER => false,
							  CURLOPT_URL => "$url?". http_build_query($request),
							  CURLOPT_RETURNTRANSFER => true,
							  CURLOPT_SSL_VERIFYPEER => false);

			$feed = curl_init();
			curl_setopt_array($feed, $options);
			$json = curl_exec($feed);
			curl_close($feed);

			return json_decode($json, true);
		}

		function timelineCall($_params){
			$user = isset($_params['user']) ? $_params['user'] : "twitterapi";
			$count = isset($_params['count']) ? $_params['count'] : "50";

			$oauth_access_token = "393597256-Xl8SUvPLbv0NFcVnYokd1EUnrih8i4zl3KPjYcb3";
			$oauth_access_token_secret = "lRKQ3OXIESoxNHR9wjbEhEToNXnrbzp70cop89pozfCOs";
			$consumer_key = "OHo6zewLwHY9y2va4ROfmrxV1";
			$consumer_secret = "jmX7pnJ5t7aHuJpHObd7ws4w3ITnvxQz2DwiICT7ZV0VoBohii";    

			$twitter_timeline           = "user_timeline";  //  mentions_timeline / user_timeline / home_timeline / retweets_of_me

			//twitterapi
			//danielbowling13
			$request = array(
				// 'trim_user'     => 1,
				'screen_name'   => $user,
				'count'         => $count
			);

			$oauth = array(
				'oauth_consumer_key'        => $this->consumer_key,
				'oauth_nonce'               => time(),
				'oauth_signature_method'    => 'HMAC-SHA1',
				'oauth_token'               => $this->oauth_access_token,
				'oauth_timestamp'           => time(),
				'oauth_version'             => '1.0'
			);

		
			$oauth = array_merge($oauth, $request);	//  merge request and oauth to one array

		//  do some magic
			$base_info              = $this->buildBaseString("https://api.twitter.com/1.1/statuses/$twitter_timeline.json", 'GET', $oauth);
			$composite_key          = rawurlencode($this->consumer_secret) . '&' . rawurlencode($this->oauth_access_token_secret);
			$oauth_signature            = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
			$oauth['oauth_signature']   = $oauth_signature;

		//  make request
			$header = array($this->buildAuthorizationHeader($oauth), 'Expect:');
			$options = array( CURLOPT_HTTPHEADER => $header,
							  CURLOPT_HEADER => false,
							  CURLOPT_URL => "https://api.twitter.com/1.1/statuses/$twitter_timeline.json?". http_build_query($request),
							  CURLOPT_RETURNTRANSFER => true,
							  CURLOPT_SSL_VERIFYPEER => false);

			$feed = curl_init();
			curl_setopt_array($feed, $options);
			$json = curl_exec($feed);
			curl_close($feed);

			return json_decode($json, true);
		}

	}


?>