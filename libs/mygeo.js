var MyGeo = {
	run: function(_func){
		var self = this;
		if(geo_position_js.init()){
			geo_position_js.getCurrentPosition(
				function(pos){
					_func({error:false,pos:pos});
				}
				,function(err){
					_func({error:true,errorObj:err});
				}
				,{
					enableHighAccuracy: true
					,options:5000
				}
			)
		}
	}
};