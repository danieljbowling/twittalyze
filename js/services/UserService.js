// public/js/services/UserService.js
angular.module('UserService', []).factory('User', ['$http', function($http) {

	return {
		// call to get all nerds

		timeline : function(_username, _callback) {
			var url = 'api/user/' + _username + '/timeline';
			// var url = 'http://prjtrakdr.energy.com.au:8080/prjtrak/api/person/' + this.username;
			$http.get(url)
				.then(function(res){
					_callback(res.data);
				})
		},

		test: function(){
			return "Test";
		}
		
	}

}]);