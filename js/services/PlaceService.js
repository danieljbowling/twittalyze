// public/js/services/PlaceService.js
angular.module('PlaceService', []).factory('Place', ['$http', function($http) {

	return {
//http://localhost:8888/twittalyze/api/place/trends/1105779
		trend: function(_place_id,_callback){
			var url = 'api/place/trends/' + _place_id;
			$http.get(url)
				.then(function(res){
					_callback(res.data);
				});
		},
		locTrend: function(_lat,_lng,_callback){
			var url = 'api/place/trends/' + _lat + '/' + _lng;
			$http.get(url)
				.then(function(res){
					_callback(res.data);
				});
		},
		geo: function(_lat,_lng,_callback){
			var url = 'api/place/geo/' + _lat + '/' + _lng;
			$http.get(url)
				.then(function(res){
					_callback(res.data);
				});
		},
		info: function(_place_id,_callback){
			var url = 'api/place/info/' + _place_id;
			$http.get(url)
				.then(function(res){
					_callback(res.data);
				});
		},
		test: function(){
			return "Test";
		}
		
	}

}]);