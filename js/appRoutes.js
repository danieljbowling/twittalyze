// public/js/appRoutes.js
	angular.module('appRoutes', []).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

	$routeProvider

		.when('/user/:username', {
			templateUrl: 'views/user.html',
			controller: 'UserController'
		})
		.when('/place/:place', {
			templateUrl: 'views/place.html',
			controller: 'PlaceController'
		})
		.when('/place', {
			templateUrl: 'views/place_search.html',
			controller: 'PlaceController'
		})
		.when('/', {
			templateUrl: 'views/home.html',
			controller: 'MainController'
		})

	$locationProvider.html5Mode(true);

}]);