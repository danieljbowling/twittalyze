// public/js/controllers/UserCtrl.js
angular.module('PlaceCtrl', []).
	controller('PlaceController', function($routeParams, $scope, $location, Place) {

	// Place.test(function(_data){
	// 	console.log( _data );
	// })

	MyGeo.run(function(result){

		if( result.error ){
			console.log( result.errorObj )
		}else{
			lat = result.pos.coords.latitude.toFixed(5);
			lng = result.pos.coords.longitude.toFixed(5);
			// console.log( lat + " " + lng );

			Place.geo( lat, lng, function(_data){
				if(_data['error']){
					console.log(_data['reason']);
				}else{
					$scope.places = _data['places']['data']['result']['places'];
				}
			});
		}

	});

	if( typeof( $routeParams.place ) != "undefined" ){
		$scope.place = $routeParams.place;

		Place.trend($scope.place,function(_data){
			$scope.trends = _data['trends']['data'][0]['trends'];
			// $scope.locations = _data['trends']['data'][0]['locations'];
			$scope.place_name = _data['trends']['data'][0]['locations'][0]['name'];
		});
	}

	$scope.selectCentroid = function(_centroid){

		Place.locTrend(_centroid[1],_centroid[0],function(_data){
			// console.log( _data['trends']['data'][0]['woeid'] );
			// console.log( _data['trends']['data'].length )

			var woeid = _data['trends']['data'][0]['woeid'];

			url = $location.path();
			url += "/" + woeid;
			$location.path( url );
		});

		// Place.info(_id,function(_data){
		// 	console.log( _data );
		// });
	}
	
});