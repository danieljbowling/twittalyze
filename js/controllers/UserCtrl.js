// public/js/controllers/UserCtrl.js
angular.module('UserCtrl', []).
	controller('UserController', function($routeParams, $scope, User) {
    
	// $scope.username = "danielbowling13";

	if( typeof( $routeParams.username ) != "undefined" ){
		
		$scope.username = $routeParams.username;
		console.log( $scope.username );

		User.timeline($scope.username,function(_data){
			// console.log( _data );
			$scope.tweet_count = _data['timeline']['tweet_count'];
			$scope.timeline_ordered = _data['timeline']['ordered'];
			$scope.drawGraph( _data['timeline']['cron_data'] );

			// console.log( $scope.timeline_ordered );
		});
	}

	$scope.drawGraph = function(_data){
		var data_bit = [];

		for(var i=0;i!=_data.length;++i){
			data_bit.push({
				data:  [ [i,0,_data[i]] ],
				label: i,
				bars: {show:true,barWidth:0.6,fill:1,align: "center"},
				color:"#2A4A70"
			})
		}

		var options = {
			bars: {
					align: "center"
				},
			legend: {
			 	show: false
			},
			grid: {
			 show : true,
			 borderWidth: 0,
			 aboveData: false,
			 margin: 10,
			 axisMargin: 10,
			 color: '#ebebeb'
			},
			shadowSize: 0
		};

		$.plot( $(".hours_graph .graphs"), data_bit , options );	
	}

});