<!-- public/index.html -->
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<base href="/">

	<title>Twittalyze</title>

	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="libs/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="libs/bootstrap/dist/css/bootstrap-responsive.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css"> <!-- custom styles -->
	<!-- JS -->	
	<script type="text/javascript" src="libs/angular/angular.min.js"></script>
	<script type="text/javascript" src="libs/angular-route/angular-route.min.js"></script>

	<!-- jQuery -->
	<script type="text/javascript" src="libs/jquery/dist/jquery.js"></script>
	<script type="text/javascript" src="libs/jquery.flot.js"></script>

	<!-- Location -->
	<script type="text/javascript" src="libs/geo.js"></script>
	<script type="text/javascript" src="libs/mygeo.js"></script>

	<!-- ANGULAR CUSTOM -->
	<script type="text/javascript" src="js/controllers/MainCtrl.js"></script>
	<script type="text/javascript" src="js/controllers/UserCtrl.js"></script>
	<script type="text/javascript" src="js/controllers/PlaceCtrl.js"></script>

	<script type="text/javascript" src="js/services/UserService.js"></script>
	<script type="text/javascript" src="js/services/PlaceService.js"></script>

	<script type="text/javascript" src="js/appRoutes.js"></script>
	<script type="text/javascript" src="js/app.js"></script>

</head>
<body ng-app="sampleApp" ng-controller="MainController">
<div class="container">

	<!-- HEADER -->
	<nav class="navbar navbar-inverse">
		<div class="navbar-header">
			<a class="navbar-brand" href="/">Twittalyze</a>
		</div>

		<!-- LINK TO OUR PAGES. ANGULAR HANDLES THE ROUTING HERE -->
		
		<ul class="nav navbar-nav">
			<li><a href="place">Places Trends</a></li>
			<li><a href="place/1105779">Sydney Trends</a></li>
		</ul>
		
	</nav>

	<!-- ANGULAR DYNAMIC CONTENT -->
	<div ng-view></div>

</div>
</body>
</html>